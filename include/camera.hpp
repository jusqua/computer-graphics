// clang-format off
#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

enum CAMERA_MOVEMENT {
  FORWARD,
  BACKWARD,
  LEFT,
  RIGHT
};

const float DEFAULT_YAW = -90.0f;
const float DEFAULT_PITCH = 0.0f;
const float DEFAULT_SPEED = 2.5f;
const float DEFAULT_FOV = 70.0f;
const float DEFAULT_SENSITIVITY = 0.1f;

const float MIN_FOV = 10.0f;
const float MAX_FOV = 115.0f;

class Camera {
public:
  glm::vec3 position;
  glm::vec3 front;
  glm::vec3 up;
  glm::vec3 right;
  glm::vec3 worldUp;

  float yaw;
  float pitch;
  float movementSpeed;
  float cursorSensitivity;
  float fov;

  Camera(
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
    float yaw = DEFAULT_YAW,
    float pitch = DEFAULT_PITCH
  );

  Camera(
    float posX = 0.0f,
    float posY = 0.0f,
    float posZ = 0.0f,
    float upX = 0.0f,
    float upY = 1.0f,
    float upZ = 0.0f,
    float yaw = DEFAULT_YAW,
    float pitch = DEFAULT_PITCH
  );

  glm::mat4 getView(void);
  void processKeyboardEvents(CAMERA_MOVEMENT type, float deltaTime);
  void processCursorEvents(float xoffset, float yoffset, bool constrainPitch = true);
  void processScrollEvents(float yoffset);

private:
  void updateVectors(void);
};

#endif
