// clang-format off
#ifndef SHADER_HPP
#define SHADER_HPP

#include <GL/glew.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

enum ShaderEnum { VERTEX, FRAGMENT, PROGRAM };

class Shader {
public:
  unsigned int ID;

  Shader(const char *vertexPath, const char *fragmentPath);

  void activate(void);
  void destroy(void);

private:
  void checkCompileError(unsigned int shader, ShaderEnum type);
};

#endif
