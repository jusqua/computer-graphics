# Learning OpenGL

Learning OpenGl from [learnopengl.com](https://learnopengl.com)

## Setting up

### Fedora

```
sudo dnf install glew-devel glfw-devel glm-devel stb-devel clang make
```

### Debian/Ubuntu

```
sudo apt install libglew-dev libglfw3-dev libglm-dev libstb-dev clang make
```

## Building

```
make build
```
