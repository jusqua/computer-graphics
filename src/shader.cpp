// clang-format off
#include <shader.hpp>

Shader::Shader(const char *vertexShaderPath, const char *fragmentShaderPath) {
  std::string vertexShaderSource, fragmentShaderSource;
  std::ifstream vertexShaderFile, fragmentShaderFile;

  vertexShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  fragmentShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

  try {
    vertexShaderFile.open(vertexShaderPath);
    fragmentShaderFile.open(fragmentShaderPath);

    std::stringstream vertexShaderStream, fragmentShaderStream;
    vertexShaderStream << vertexShaderFile.rdbuf();
    fragmentShaderStream << fragmentShaderFile.rdbuf();

    vertexShaderFile.close();
    fragmentShaderFile.close();

    vertexShaderSource = vertexShaderStream.str();
    fragmentShaderSource = fragmentShaderStream.str();
  }
  catch (std::ifstream::failure& e) {
    std::cerr << "ERROR: Failed to source shaders\n" << e.what() << std::endl;
  }

  const char *vertexShaderCode = vertexShaderSource.c_str();
  const char *fragmentShaderCode = fragmentShaderSource.c_str();
  unsigned int vertexShader, fragmentShader;

  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexShaderCode, NULL);
  glCompileShader(vertexShader);
  checkCompileError(vertexShader, ShaderEnum::VERTEX);

  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentShaderCode, NULL);
  glCompileShader(fragmentShader);
  checkCompileError(fragmentShader, ShaderEnum::FRAGMENT);

  this->ID = glCreateProgram();
  glAttachShader(this->ID, vertexShader);
  glAttachShader(this->ID, fragmentShader);
  glLinkProgram(this->ID);
  checkCompileError(this->ID, ShaderEnum::PROGRAM);

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
}

void Shader::activate(void) {
  glUseProgram(this->ID);
}

void Shader::destroy(void) {
  glDeleteProgram(this->ID);
}

void Shader::checkCompileError(unsigned int shader, ShaderEnum type) {
  int success;
  char log[512];

  switch(type) {
    case PROGRAM:
      glGetProgramiv(shader, GL_LINK_STATUS, &success);
      if (!success) {
        glGetProgramInfoLog(shader, 512, NULL, log);
        std::cerr << "ERROR: Failed to link shaders\n" << log << std::endl;
      }
      break;
    case VERTEX:
    case FRAGMENT:
      glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
      if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, log);
        std::cerr << "ERROR: Failed to compile shader\n" << log << std::endl;
      }
      break;
    default:
      std::cerr << "ERROR: Something gone wrong" << std::endl;
      break;
  }
}
