#version 330
layout (location = 0) in vec3 in_Position;
layout (location = 1) in vec2 in_Texture;
out vec2 ex_Texture;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void) {
  gl_Position = projection * view * model * vec4(in_Position, 1.0f);
  ex_Texture = in_Texture;
}
