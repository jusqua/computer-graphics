#version 330
in vec2 ex_Texture;
out vec4 out_Color;
uniform sampler2D texture0;
uniform sampler2D texture1;

void main(void) {
  out_Color = mix(
    texture(texture0, ex_Texture),
    texture(texture1, ex_Texture),
    0.2
  );
}
